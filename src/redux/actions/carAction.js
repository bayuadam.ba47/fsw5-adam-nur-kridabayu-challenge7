import axios from "axios";

//Membuatan konstan
export const GET_ALL_CARS = "GET_ALL_CARS";

export const getAllCars = () => {
  console.log("2. Ini dari Action");
  //Dispatch = penghubung
  return (dispatch) => {
    dispatch({
      type: GET_ALL_CARS,
      payload: {
        loading: true,
        data: false,
        errorMessage: false,
      },
    });

    //get API
    axios({
      method: "GET",
      url: "https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/data/cars.min.json",
    })
      .then((response) => {
        console.log("3. dapet data:", response);
        //berhasil get api
        dispatch({
          type: GET_ALL_CARS,
          payload: {
            loading: false,
            data: response.data,
            errorMessage: false,
          },
        });
      })
      .catch((error) => {
        console.log("3. gagal data : ", error);
        dispatch({
          type: GET_ALL_CARS,
          payload: {
            loading: false,
            data: false,
            errorMessage: error.message,
          },
        });
      });
  };
};
