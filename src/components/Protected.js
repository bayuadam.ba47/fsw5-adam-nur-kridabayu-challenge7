import { Navigate } from "react-router-dom";

// TODO: Leave it to your imagination
function Protected({ children }) {
  const loginData = localStorage.getItem("loginData");

  if (!loginData) return <Navigate to="/login" />;

  return children;
}

export default Protected;
