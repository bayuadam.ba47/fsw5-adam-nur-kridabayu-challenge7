const CarFilter = (cars, { type, date, pickupTime, pass }) => {
  return (
    cars
      // Filter data.available
      .filter((car) => car.available === true)
      .filter((car) => {
        // Filter data.type Driver
        if (type === "Keyless Entry") {
          if (car.options.includes(type)) {
            return car;
          }
        } else {
          return !car.options.includes("Keyless Entry");
        }
      })
      .filter((car) => {
        // Filter pilih tanggal
        const dateCar = new Date(car.availableAt);
        const datePicked = new Date(date);
        if (dateCar >= datePicked) {
          return car;
        }
      })
      .filter((car) => {
        // Filter Waktu
        let dateCar = new Date(car.availableAt).getHours();
        if (dateCar >= Number(pickupTime)) {
          return car;
        }
      })
      //Filter penumpang
      .filter((car) => car.capacity >= pass)
  );
};

module.exports = { CarFilter };
